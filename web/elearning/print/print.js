/* App Main File */

/*jslint plusplus: true */
/*global angular: true, console: true */

angular.module('printApp', [])
  .filter('translate', function () {
    'use strict';

    var i18n;
  
    // German as key and English as value.
    i18n = {
      'Ihre Stressoren-Ressourcen-Bilanz': 'Your Stressor-Resource Balance',
      'Ihre Ressourcen': 'Your Resources',
      'Ihre Stressoren': 'Your Stressors',
      'Arbeits\u00adinhalte': 'work content',
      'Arbeits\u00adorganisation': 'work organization',
      'Arbeits\u00adumgebung': 'work environment',
      'Soziale Aspekte': 'social relations',
      'Innere Ressourcen': 'internal resources',
      'Innere Stressoren': 'internal stressors'
    };
  
    return function (key, language) {
      if (language === 'EN' && i18n[key]) {
        return i18n[key];
      }

      // Else: Just return the key.

      return key;
    };
  })
  .controller('printController', [
    '$scope',
    '$location',
    '$timeout',
    function ($scope, $location, $timeout) {
      'use strict';
      var searchObject;
      
      $scope.$on('$locationChangeSuccess', function () {
        searchObject = $location.search();
      
        if (searchObject.data) {
          $scope.data = angular.fromJson(searchObject.data);
          
          // Set language
          $scope.language = $scope.data.language;

          $timeout(function () {
            window.print();
          }, 50);
        }
      });
    }]);