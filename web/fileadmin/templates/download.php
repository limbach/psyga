<?php
$file = '/'.$_REQUEST['dl'];

$suffix = explode("/",$file);
$path = '/';
foreach($suffix as $key => $fix){
    if(trim($fix)!=''&&$key!=count($suffix)-1)  $path .= $fix.'/';
    if($key==count($suffix)-1)  $file = $fix;
}

$suffix = explode(".",$file);

switch(strtolower($suffix[count($suffix)-1])){
	case 'pps':
		$contenttypedatei='application/vnd.ms-powerpoint';
		break;
	##### Microsoft Powerpoint Dateien
	case 'doc':
		$contenttypedatei='application/msword';
		break;
	##### Microsoft Word Dateien
	case 'xls':
		$contenttypedatei='application/vnd.ms-excel';
		break;
	##### Microsoft Excel Dateien
	case 'jpeg':
		$contenttypedatei='image/jpeg';
		break;
	##### JPEG-Dateien
	case 'jpg':
		$contenttypedatei='image/jpeg';
		break;
	##### JPEG-Dateien
	case 'jpe':
		$contenttypedatei='image/jpeg';
		break;
	##### JPEG-Dateien
	case 'mpeg':
		$contenttypedatei='video/mpeg';
		break;
	##### MPEG-Dateien
	case 'mpg':
		$contenttypedatei='video/mpeg';
		break;
	##### MPEG-Dateien
	case 'mpe':
		$contenttypedatei='video/mpeg';
		break;
	##### MPEG-Dateien
	case 'mov':
		$contenttypedatei='video/quicktime';
		break;
	##### Quicktime-Dateien
	case 'avi':
		$contenttypedatei='video/x-msvideo';
		break;
	##### Microsoft AVI-Dateien
	case 'pdf':
		$contenttypedatei='application/pdf';
		break;
	case 'svg':
		$contenttypedatei='image/svg+xml';
		break;
	### Flash Video Files
	case 'flv':
		$contenttypedatei='video/x-flv';
		break;
	### Shockwave / Flash
	case 'swf':
		$contenttypedatei='application/x-shockwave-flash';
		break;
	case 'mp3':
	    $contenttypedatei='audio/mpeg, audio/x-mpeg, audio/x-mpeg-3, audio/mpeg3';
	    break;
    case 'html':
	    $contenttypedatei='text/html';
	    break;
	default:
//			$contenttypedatei='application/octet-stream';
		break;
}//end of switch Case structure

header("Pragma: private");
header("Expires: 0"); // set expiration time
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header('Content-Type: '.$contenttypedatei);
header('Content-Disposition: attachment; filename="'.$file.'"');
readfile($_SERVER['DOCUMENT_ROOT'].$path.$file);

function show($x){
    echo '<pre>';
    print_r($x);
    echo '</pre>';
}
?>