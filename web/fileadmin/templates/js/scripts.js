jQuery(function( $ ){
    /*$(function() {
        $('a[href$=".pdf"]').each(function() {
            $(this).attr( "onmousedown", "ET_Event.download( '" + $(this).attr( 'href' ) + "', '')" )
        });
    });*/

    /* NAVIGATION */
    $("#navigation>ul>li:last").css({"position":"absolute","right":"0","z-index":"1999"}).hover(function(){
        $(this).css({"z-index":"2000"});
    }, function(){
        $(this).css({"z-index":"1999"});
    });
    $("#navigation ul li").hover(function(){
        // display and hide flyout submenu
        // check position and correct if necessary
        if($("ul",this).length>0){
            var maxwidth = $("#container-inner").width();
            var subnavwidth = parseInt($("ul",this).width());
            var offset = parseInt($(this).offset().left - $("#navigation").offset().left);
            if( maxwidth-offset < subnavwidth ){
                // submenu to far right
//                $("ul",this).css({"left":( (maxwidth-offset)-subnavwidth)-22+"px"});
                $("ul",this).css({"right":"0"});
            }
            $(this).addClass("tabframe");
            $("ul",this).show();
        }
    }, function(){
        if($("ul",this).length>0){
            $(this).removeClass("tabframe");
            $("ul",this).hide();
        }
    });
    // fix empty image in subnavigation
    $(".foi-img").each(function(){
       if($(this).html()=='')   $(this).html("&nbsp;");
    });
    $(".subnav").each(function(){
        // remove border from two last submenu items
        $(".fo-item:last",this).css({"border":"0"}).prev().css({"border":"0"});
        // add clearing to submenu items for cleaner display
        $(":nth-child(2n+1)",this).css({"clear":"both"});
    });
    /* NAVIGATION */
    
    /* HEADERBOX EMPTY LINK FIX */
    if($("#hcategory a").attr("href")=="")    $("#hcategory").remove();
    if($("#hmore").attr("href")=="")    $("#hmore").remove();
    /* HEADERBOX EMPTY LINK FIX */
    
    /* TEASERBOX CSC-DEFAULT FIX */
    $(".teaserbox").parents(".csc-default").css({"padding":"0"});
    /* TEASERBOX CSC-DEFAULT FIX */
    
    /* LANDINGPAGE TEASER FIX */
    $(".tb-sub:odd").addClass("tbsright");
    $(".tb-sub:even").css({"clear":"both"});
    $(".tb-sub:even").each(function(){
        if( $(this).next().length<1 )   $(this).css({"padding":"15px 300px 15px 0"});
    });
    /* LANDINGPAGE TEASER FIX */
    
    /* TABBBOX TIPP EMPTY FIX */
    $("#tab-tipps ul li").each(function(){
        if($(this).html()=="")  $(this).remove();
    });
    /* TABBBOX TIPP EMPTY FIX */
    
    /* TAB BOX */
    if($("#tabbox").length>0){

        if($.browser.chrome==true)  $("#tabbox .tb-body").css({"top":($("#tabbox .tb-head").height()-2)+"px"});
        
        $(".tb-head a").click(function(){
            $(".tb-body").hide();
            $(".tb-head a").removeClass("active");
            $(this).addClass("active").parent().next().show();
            return false;
        });
        $("#tab-service .tb-shead a").click(function(){
            var activeclass = "active";
            var head = $(this).parent().parent();
            if(!head.hasClass(activeclass)){
                $(".tb-sbody").hide().prev().removeClass(activeclass);
                head.addClass(activeclass).next().show();
            }
            return false;
        });
    }
    if($(".accordeon").length>0){
        $(".accordeon").each(function(){
//            $(".tb-sitem:first",this).addClass("active").find(".tb-sbody").css({"display":"block"});
            $(".tb-sitem:last",this).addClass("last");
        });
        $(".accordeon .tb-head a").click(function(){
//            $(".accordeon .tb-body").hide();
//            $(" .tb-head a").removeClass("active");
//            $(this).addClass("active").parent().next().show();
            return false;
        });
        $(".accordeon .tb-shead a").click(function(){
            var activeclass = "active";
            var head = $(this).parent().parent();
            if(!head.hasClass(activeclass)){
//                $(".accordeon .tb-sbody").hide().prev().removeClass(activeclass);
                head.addClass(activeclass).next().show();
            } else {
                head.removeClass(activeclass).next().hide();
            }
            return false;
        });
    }
    /* TAB BOX */
    
    /* SEARCH */
    $("#hsearch").each(function(){
        var def = $(this).val();
        $(this).focus(function(){
            if($(this).val()==def)   $(this).val("");
        }).blur(function(){
            if($(this).val()=='')   $(this).val(def);
        });
    });
    $(".tx-indexedsearch-result").parent().attr("id","indexedsearch-res");
    $(".tx-indexedsearch-resultbody a").each(function(){
        $(this).attr("href",$(this).parent().prev().find('a').attr("href"));
    });
    /* SEARCH */
    
    /* FORCE DOWNLOADS */
    $("a.download").click(function(){
        $(this).attr("target","").attr("href",'fileadmin/templates/download.php?dl='+$(this).attr("href"));
    });
    /* FORCE DOWNLOADS */
    
    /* MULTIMEDIA TAB */
    $(".playbutton a").click(function() {
        if($(this).attr("href")!=''){
            $.fancybox({
                'padding' : 10,
                'autoScale' : false,
                'transitionIn' : 'none',
                'transitionOut' : 'none',
                'title' : this.title,
                'width' : 680,
                'height' : 495,
                'href' : this.href.replace(new RegExp("([0-9])","i"),'moogaloop.swf?clip_id=$1'), // youtube: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'), // vimeo: this.href.replace(new RegExp("([0-9])","i"),'moogaloop.swf?clip_id=$1'),
                'type' : 'swf',
                'swf' : {
                    'wmode' : 'transparent',
                    'allowfullscreen' : 'true'
                }
            });
        }
        return false;
    });
    /* MULTIMEDIA TAB */
    
    /* NEW PUBLICATION */
    $(".new-pub").click(function(){ if($("a",this).length>0)    $(location).attr('href', $("a:first",this).attr("href") );  });
    /* NEW PUBLICATION */
	
	$('.elearning').click(function() {
		window.open(this.href,'popup','height=780,width=1100');
		return false
	}
	);


	
});
function openpopup (url, paramW, paramH) {
    strPopup = "width="+paramW+", height="+paramH+", status=yes, scrollbars=no, resizable=no";
    fenster = window.open(url, "fenster1", strPopup );
    fenster.focus();
}








$(document).ready(function () {
    $('.zitate_slider').each(function(){
        var ziz = $(this).find('.swiper-container');
        console.log(ziz.find('.swiper-slide').size());
        if(ziz.find('.swiper-slide').size() > 1){
            var mySwiper = new Swiper (ziz, {
                loop: true,
                autoplay: 3500,
                speed: 2000,
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev'
            }); 
        } else {
            ziz.find('.swiper-button-next').hide();
            ziz.find('.swiper-button-prev').hide();
        }
    });
});