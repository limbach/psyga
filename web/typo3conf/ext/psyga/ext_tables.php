<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}
$tempColumns = array(
	'tx_psyga_teasertitle' => array(		
		'exclude' => 0,		
		'label' => 'LLL:EXT:psyga/locallang_db.xml:pages.tx_psyga_teasertitle',		
		'config' => array(
			'type' => 'input',	
			'size' => '30',	
			'eval' => 'trim',
		)
	),
	'tx_psyga_teasersubtitle' => array(		
		'exclude' => 0,		
		'label' => 'LLL:EXT:psyga/locallang_db.xml:pages.tx_psyga_teasersubtitle',		
		'config' => array(
			'type' => 'input',	
			'size' => '30',	
			'eval' => 'trim',
		)
	),
);


t3lib_div::loadTCA('pages');
t3lib_extMgm::addTCAcolumns('pages',$tempColumns,1);
t3lib_extMgm::addToAllTCAtypes('pages','tx_psyga_teasertitle;;;;1-1-1, tx_psyga_teasersubtitle','','after:subtitle');
?>