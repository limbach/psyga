<?php

########################################################################
# Extension Manager/Repository config file for ext "dix_urltool".
#
# Auto generated 04-11-2012 20:00
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'UrlTool',
	'description' => 'Manage 404 Error-Handling / RealUrl-Configuration / Realurl-Cache',
	'category' => 'module',
	'shy' => 0,
	'version' => '0.1.2',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => 'mod1',
	'state' => 'beta',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearcacheonload' => 0,
	'lockType' => '',
	'author' => 'Markus Kappe, Andreas Eberhard',
	'author_email' => 'markus.kappe@dix.at, aeberhard@users.sourceforge.net',
	'author_company' => '',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'constraints' => array(
		'depends' => array(
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:12:{s:9:"ChangeLog";s:4:"8549";s:12:"ext_icon.gif";s:4:"f9b3";s:14:"ext_tables.php";s:4:"9fe1";s:10:"README.txt";s:4:"1f05";s:14:"doc/manual.sxw";s:4:"cc81";s:14:"mod1/clear.gif";s:4:"cc11";s:13:"mod1/conf.php";s:4:"2cd8";s:23:"mod1/defaultrealurl.txt";s:4:"d6c8";s:14:"mod1/index.php";s:4:"c341";s:18:"mod1/locallang.xml";s:4:"198d";s:22:"mod1/locallang_mod.xml";s:4:"b134";s:19:"mod1/moduleicon.gif";s:4:"bb61";}',
);

?>