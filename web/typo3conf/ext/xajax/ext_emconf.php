<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "xajax".
 *
 * Auto generated 25-06-2013 15:31
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'xaJax',
	'description' => 'This is a 0.2.x version of the xajax library. This extension will not be migrated to 0.5.x because the API has changed and several extensions depend on this old API. To use the current API please have a look for another extension in TER. More documentation in the extension xajax_tutor.',
	'category' => 'misc',
	'shy' => 0,
	'version' => '0.2.6',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => '',
	'state' => 'stable',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearcacheonload' => 0,
	'lockType' => '',
	'author' => 'Elmar Hinz, Jared White, J. Max Wilson',
	'author_email' => 't3elmar@googelmail.com',
	'author_company' => 'Freelancer',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'constraints' => array(
		'depends' => array(
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:11:{s:9:"ChangeLog";s:4:"4308";s:18:"class.tx_xajax.php";s:4:"9cfc";s:27:"class.tx_xajax_response.php";s:4:"203c";s:12:"ext_icon.gif";s:4:"3ec5";s:8:"icon.gif";s:4:"4aa7";s:11:"LICENSE.txt";s:4:"0cce";s:10:"README.txt";s:4:"8362";s:17:"xajaxCompress.php";s:4:"9399";s:14:"doc/manual.sxw";s:4:"1dd8";s:17:"xajax_js/xajax.js";s:4:"0b50";s:30:"xajax_js/xajax_uncompressed.js";s:4:"8e5d";}',
	'suggests' => array(
	),
);

?>