/* App Main File */

/*jslint plusplus: true */
/*global angular: true, cordova: true, StatusBar: true, console: true */

// Create module objects.
angular.module('eLearningApp.controllers', []);
angular.module('eLearningApp.directives', []);
angular.module('eLearningApp.services', []);
angular.module('eLearningApp.filters', []);

// Define app module.
angular.module('eLearningApp', ['ionic', 'eLearningApp.controllers', 'eLearningApp.services', 'eLearningApp.directives', 'eLearningApp.filters'])
  .run(['$ionicPlatform', function ($ionicPlatform) {
    'use strict';
  
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      
      // TODO Fix status bar issue on iOS.
      /*setTimeout(function () {
        // window.statusBar seems to be missing (at least using Ionic View)
        // Using a timeout fixes this issue, but this is unsatisfactory.
        if (!window.StatusBar) {
          alert('StatusBar missing');
        } else {
          alert('StatusBar present');
        }

        // Hide the status bar on iOS devices on app start up.
        //$ionicPlatform.showStatusBar(false);
        // OR
        //$ionicPlatform.fullScreen();
        
        // Remove this horrible hack as soon as possible.
        document.body.classList.add('fullscreen');
        window.StatusBar.hide();
        document.body.classList.add('status-bar-hide');
      }, 1000);*/
    });
  }])
  .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    'use strict';
    var makeTemplateProvider;
  
    makeTemplateProvider = function (path) {
      return ['$templateCache',
              '$templateFactory',
              'storage',
              function ($templateCache, $templateFactory, storage) {
          var template, settings, newPath;
          
          if (storage.getItem('settings')) {
            settings = storage.getItem('settings');
          } else {
            // Set default language.
            settings = {language: 'DE'};

            // Wire up with storage service.
            storage.setItem('settings', settings);
          }
          
          // Special handling for English.
          if (settings.language === 'EN') {
            newPath = 'en/' + path;
          } else {
            newPath = path;
          }
          
          template = $templateCache.get(newPath);
          
          if (!template) {
            return $templateFactory.fromUrl(newPath)
              .then(function (template) {
                return template;
              });
          } else {
            // Only return string of template cache.
            return template[1];
          }
        }];
    };
    
    $stateProvider
      .state('intro', {
        url: "/intro",
        templateUrl: "templates/intro.html",
        controller: 'IntroCtrl'
      })
      .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "templates/menu.html",
        controller: 'AppCtrl'
      })
      .state('app.slide', {
        url: "/slide",
        views: {
          'menuContent': {
            templateUrl: "templates/slide.html",
            controller: 'SlideController'
          }
        }
      })
      .state('app.balance', {
        url: "/balance",
        abstract: true,
        views: {
          'menuContent': {
            templateUrl: "templates/content-view.html"
          }
        }
      })
      .state('app.balance.tips', {
        url: "/tips",
        templateProvider: makeTemplateProvider('templates/balance/tips.html')
      })
      .state('app.balance.summary', {
        url: "/summary",
        templateProvider: makeTemplateProvider('templates/balance/summary.html')
      })
      .state('app.balance.conclusion', {
        url: "/conclusion",
        templateProvider: makeTemplateProvider('templates/balance/conclusion.html')
      })
      .state('app.guidance', {
        url: "/guidance",
        abstract: true,
        views: {
          'menuContent': {
            templateUrl: "templates/content-view.html"
          }
        }
      })
      .state('app.guidance.start', {
        url: "/start",
        templateProvider: makeTemplateProvider('templates/guidance/start.html')
      })
      .state('app.guidance.external-resources-overview', {
        url: "/external-resources/overview",
        templateProvider: makeTemplateProvider('templates/guidance/external-resources/overview.html')
      })
      .state('app.guidance.external-resources-participate', {
        url: "/external-resources/participate",
        templateProvider: makeTemplateProvider('templates/guidance/external-resources/participate.html')
      })
      .state('app.guidance.external-resources-social-network-1', {
        url: "/external-resources/social-network/1",
        templateProvider: makeTemplateProvider('templates/guidance/external-resources/social-network-1.html')
      })
      .state('app.guidance.external-resources-social-network-2', {
        url: "/external-resources/social-network/2",
        templateProvider: makeTemplateProvider('templates/guidance/external-resources/social-network-2.html')
      })
      .state('app.guidance.external-resources-working-atmosphere', {
        url: "/external-resources/working-atmosphere",
        templateProvider: makeTemplateProvider('templates/guidance/external-resources/working-atmosphere.html')
      })
      .state('app.guidance.external-resources-appreciation-1', {
        url: "/external-resources/appreciation/1",
        templateProvider: makeTemplateProvider('templates/guidance/external-resources/appreciation-1.html')
      })
      .state('app.guidance.external-resources-appreciation-2', {
        url: "/external-resources/appreciation/2",
        templateProvider: makeTemplateProvider('templates/guidance/external-resources/appreciation-2.html')
      })
      .state('app.guidance.external-resources-appreciation-3', {
        url: "/external-resources/appreciation/3",
        templateProvider: makeTemplateProvider('templates/guidance/external-resources/appreciation-3.html')
      })
      .state('app.guidance.internal-resources-overview', {
        url: "/internal-resources/overview",
        templateProvider: makeTemplateProvider('templates/guidance/internal-resources/overview.html')
      })
      .state('app.guidance.internal-resources-enjoyment-training-1', {
        url: "/internal-resources/enjoyment-training/1",
        templateProvider: makeTemplateProvider('templates/guidance/internal-resources/enjoyment-training-1.html')
      })
      .state('app.guidance.internal-resources-enjoyment-training-2', {
        url: "/internal-resources/enjoyment-training/2",
        templateProvider: makeTemplateProvider('templates/guidance/internal-resources/enjoyment-training-2.html')
      })
      .state('app.guidance.internal-resources-resilience-training-1', {
        url: "/internal-resources/resilience-training/1",
        templateProvider: makeTemplateProvider('templates/guidance/internal-resources/resilience-training-1.html')
      })
      .state('app.guidance.internal-resources-resilience-training-2', {
        url: "/internal-resources/resilience-training/2",
        templateProvider: makeTemplateProvider('templates/guidance/internal-resources/resilience-training-2.html')
      })
      .state('app.guidance.internal-resources-recovering-1', {
        url: "/internal-resources/recovering/1",
        templateProvider: makeTemplateProvider('templates/guidance/internal-resources/recovering-1.html')
      })
      .state('app.guidance.internal-resources-recovering-2', {
        url: "/internal-resources/recovering/2",
        templateProvider: makeTemplateProvider('templates/guidance/internal-resources/recovering-2.html')
      })
      .state('app.guidance.internal-resources-recovering-3', {
        url: "/internal-resources/recovering/3",
        templateProvider: makeTemplateProvider('templates/guidance/internal-resources/recovering-3.html')
      })
      .state('app.guidance.internal-resources-recovering-4', {
        url: "/internal-resources/recovering/4",
        templateProvider: makeTemplateProvider('templates/guidance/internal-resources/recovering-4.html')
      })
      .state('app.guidance.internal-resources-recovering-5', {
        url: "/internal-resources/recovering/5",
        templateProvider: makeTemplateProvider('templates/guidance/internal-resources/recovering-5.html')
      })
      .state('app.guidance.internal-resources-recovering-6', {
        url: "/internal-resources/recovering/6",
        templateProvider: makeTemplateProvider('templates/guidance/internal-resources/recovering-6.html')
      })
      .state('app.guidance.internal-resources-recovering-7', {
        url: "/internal-resources/recovering/7",
        templateProvider: makeTemplateProvider('templates/guidance/internal-resources/recovering-7.html')
      })
      .state('app.guidance.internal-resources-success-diary', {
        url: "/internal-resources/success-diary",
        templateProvider: makeTemplateProvider('templates/guidance/internal-resources/success-diary.html')
      })
      .state('app.guidance.internal-resources-physical-health-1', {
        url: "/internal-resources/physical-health/1",
        templateProvider: makeTemplateProvider('templates/guidance/internal-resources/physical-health-1.html')
      })
      .state('app.guidance.internal-resources-physical-health-2', {
        url: "/internal-resources/physical-health/2",
        templateProvider: makeTemplateProvider('templates/guidance/internal-resources/physical-health-2.html')
      })
      .state('app.guidance.internal-resources-physical-health-3', {
        url: "/internal-resources/physical-health/3",
        templateProvider: makeTemplateProvider('templates/guidance/internal-resources/physical-health-3.html')
      })
      .state('app.guidance.external-stressors-overview', {
        url: "/external-stressors/overview",
        templateProvider: makeTemplateProvider('templates/guidance/external-stressors/overview.html')
      })
      .state('app.guidance.external-stressors-solve-training', {
        url: "/external-stressors/solve-training",
        templateProvider: makeTemplateProvider('templates/guidance/external-stressors/solve-training.html')
      })
      .state('app.guidance.external-stressors-self-motivation-1', {
        url: "/external-stressors/self-motivation/1",
        templateProvider: makeTemplateProvider('templates/guidance/external-stressors/self-motivation-1.html')
      })
      .state('app.guidance.external-stressors-self-motivation-2', {
        url: "/external-stressors/self-motivation/2",
        templateProvider: makeTemplateProvider('templates/guidance/external-stressors/self-motivation-2.html')
      })
      .state('app.guidance.external-stressors-self-motivation-3', {
        url: "/external-stressors/self-motivation/3",
        templateProvider: makeTemplateProvider('templates/guidance/external-stressors/self-motivation-3.html')
      })
      .state('app.guidance.external-stressors-accessibility-delusional', {
        url: "/external-stressors/accessibility-delusional",
        templateProvider: makeTemplateProvider('templates/guidance/external-stressors/accessibility-delusional.html')
      })
      .state('app.guidance.external-stressors-no-praise', {
        url: "/external-stressors/no-praise",
        templateProvider: makeTemplateProvider('templates/guidance/external-stressors/no-praise.html')
      })
      .state('app.guidance.external-stressors-solve-conflicts-1', {
        url: "/external-stressors/solve-conflicts/1",
        templateProvider: makeTemplateProvider('templates/guidance/external-stressors/solve-conflicts-1.html')
      })
      .state('app.guidance.external-stressors-solve-conflicts-2', {
        url: "/external-stressors/solve-conflicts/2",
        templateProvider: makeTemplateProvider('templates/guidance/external-stressors/solve-conflicts-2.html')
      })
      .state('app.guidance.external-stressors-conflict-talks-1', {
        url: "/external-stressors/conflict-talks/1",
        templateProvider: makeTemplateProvider('templates/guidance/external-stressors/conflict-talks-1.html')
      })
      .state('app.guidance.external-stressors-conflict-talks-2', {
        url: "/external-stressors/conflict-talks/2",
        templateProvider: makeTemplateProvider('templates/guidance/external-stressors/conflict-talks-2.html')
      })
      .state('app.guidance.external-stressors-criticism-talks-1', {
        url: "/external-stressors/criticism-talks/1",
        templateProvider: makeTemplateProvider('templates/guidance/external-stressors/criticism-talks-1.html')
      })
      .state('app.guidance.external-stressors-criticism-talks-2', {
        url: "/external-stressors/criticism-talks/2",
        templateProvider: makeTemplateProvider('templates/guidance/external-stressors/criticism-talks-2.html')
      })
      .state('app.guidance.external-stressors-criticism-talks-3', {
        url: "/external-stressors/criticism-talks/3",
        templateProvider: makeTemplateProvider('templates/guidance/external-stressors/criticism-talks-3.html')
      })
      .state('app.guidance.external-stressors-self-contract', {
        url: "/external-stressors/self-contract",
        templateProvider: makeTemplateProvider('templates/guidance/external-stressors/self-contract.html')
      })
      .state('app.guidance.internal-stressors-overview', {
        url: "/internal-stressors/overview",
        templateProvider: makeTemplateProvider('templates/guidance/internal-stressors/overview.html')
      })
      .state('app.guidance.internal-stressors-shortbreak-1', {
        url: "/internal-stressors/shortbreak-1",
        templateProvider: makeTemplateProvider('templates/guidance/internal-stressors/shortbreak-1.html')
      })
      .state('app.guidance.internal-stressors-shortbreak-2', {
        url: "/internal-stressors/shortbreak-2",
        templateProvider: makeTemplateProvider('templates/guidance/internal-stressors/shortbreak-2.html')
      })
      .state('app.guidance.internal-stressors-selfesteem-1', {
        url: "/internal-stressors/selfesteem/1",
        templateProvider: makeTemplateProvider('templates/guidance/internal-stressors/selfesteem-1.html')
      })
      .state('app.guidance.internal-stressors-selfesteem-2', {
        url: "/internal-stressors/selfesteem/2",
        templateProvider: makeTemplateProvider('templates/guidance/internal-stressors/selfesteem-2.html')
      })
      .state('app.guidance.internal-stressors-pareto', {
        url: "/internal-stressors/pareto",
        templateProvider: makeTemplateProvider('templates/guidance/internal-stressors/pareto.html')
      })
      .state('app.guidance.internal-stressors-parkinson', {
        url: "/internal-stressors/parkinson",
        templateProvider: makeTemplateProvider('templates/guidance/internal-stressors/parkinson.html')
      })
      .state('app.guidance.internal-stressors-alpen', {
        url: "/internal-stressors/alpen",
        templateProvider: makeTemplateProvider('templates/guidance/internal-stressors/alpen.html')
      })
      .state('app.guidance.internal-stressors-column-1', {
        url: "/internal-stressors/column/1",
        templateProvider: makeTemplateProvider('templates/guidance/internal-stressors/column-1.html')
      })
      .state('app.guidance.internal-stressors-column-2', {
        url: "/internal-stressors/column/2",
        templateProvider: makeTemplateProvider('templates/guidance/internal-stressors/column-2.html')
      })
      .state('app.guidance.internal-stressors-column-3', {
        url: "/internal-stressors/column/3",
        templateProvider: makeTemplateProvider('templates/guidance/internal-stressors/column-3.html')
      })
      .state('app.guidance.internal-stressors-limits-1', {
        url: "/internal-stressors/limits/1",
        templateProvider: makeTemplateProvider('templates/guidance/internal-stressors/limits-1.html')
      })
      .state('app.guidance.internal-stressors-limits-2', {
        url: "/internal-stressors/limits/2",
        templateProvider: makeTemplateProvider('templates/guidance/internal-stressors/limits-2.html')
      })
      .state('app.guidance.internal-stressors-limits-3', {
        url: "/internal-stressors/limits/3",
        templateProvider: makeTemplateProvider('templates/guidance/internal-stressors/limits-3.html')
      })
      .state('app.guidance.internal-stressors-roadmap-1', {
        url: "/internal-stressors/roadmap/1",
        templateProvider: makeTemplateProvider('templates/guidance/internal-stressors/roadmap-1.html')
      })
      .state('app.guidance.internal-stressors-roadmap-2', {
        url: "/internal-stressors/roadmap/2",
        templateProvider: makeTemplateProvider('templates/guidance/internal-stressors/roadmap-2.html')
      })
      .state('app.guidance.internal-stressors-my-stress', {
        url: "/internal-stressors/my-stress",
        templateProvider: makeTemplateProvider('templates/guidance/internal-stressors/my-stress.html')
      });
  
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/intro');
  }]);
