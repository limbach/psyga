/* Services */

/*jslint plusplus: true */
/*global angular: true, console: true */

angular.module('eLearningApp.services')
  .factory('slideService', [
    '$http',
    function ($http) {
      'use strict';
      var slideService, data;

      slideService = {};
      data = { slides: [] };

      // Load chapter data from JSON file.
      // NOTE No error handling.
      $http.get('js/chaptersShort.json').then(function (result) {
        angular.extend(data, result.data);
      });

      slideService.getSlideMetadata = function () {
        return data;
      };

      slideService.getSlideCount = function () {
        return data.slides.length;
      };

      slideService.getCurrentChapter = function (index) {
        if (!angular.isNumber(index)) {
          return null;
        }

        if (index >= 0 && index <= (data.slides.length - 1)) {
          return data.slides[index].chapter;
        } else {
          return null;
        }
      };

      slideService.getRemainingSlideCount = function (index) {
        var count, currentChapter, i;

        if (!angular.isNumber(index)) {
          return null;
        }

        if (index >= 0 && index <= (data.slides.length - 1)) {
          count = 1;
          currentChapter = data.slides[index].chapter;

          for (i = (index + 1); i < data.slides.length; i++) {
            if (data.slides[i].chapter === currentChapter) {
              count++;
            } else {
              break;
            }
          }

          return count;
        } else {
          return null;
        }
      };

      return slideService;
    }]);