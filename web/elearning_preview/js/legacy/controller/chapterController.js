/* Chapter Controller */

/*jslint plusplus: true */
/*global angular: true, $: true, Modernizr : true, console: true */

angular.module('eLearningApp.controllers')
  .controller('ChapterController', [
    '$scope',
    '$stateParams',
    '$location',
    function ($scope, $stateParams, $location) {
      'use strict';
      var slideNumber, pageIndex;
      
      if (angular.isDefined($stateParams.slide)) {
        slideNumber = parseInt($stateParams.slide, 10);
        
        if (angular.isNumber(slideNumber)) {
          // Correct page index.
          pageIndex = slideNumber - 1;
          if ($scope.slideIndex !== pageIndex) {
            $scope.goToSlide(pageIndex);
          } else {
            // At least update the chapter classes!
            $scope.updateChapterClass();
          }
        }
      } else if ($scope.slideIndex > 0) {
        // Make sure the slide url parameter is updated.
        $location.search({ 'slide': $scope.slideIndex + 1 });
        $scope.updateChapterClass();
      }

    }]);