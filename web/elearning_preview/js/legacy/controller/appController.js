/* App Controller */

/*jslint plusplus: true */
/*global angular: true, $: true, Modernizr : true, console: true */

angular.module('eLearningApp.controllers')
  .controller('AppController', [
    '$scope',
    'slideService',
    '$timeout',
    '$state',
    '$location',
    '$http',
    '$compile',
    function ($scope, slideService, $timeout, $state, $location, $http, $compile) {
      'use strict';
      var updateChapterClass, lastChapter, deferredSlideIndex, modalObject;

      $scope.isFirstSlide = true;
      $scope.isLastSlide = true;
      $scope.slideIndex = 0;
      $scope.currentChapterClass = null;
      
      if ($state.current.name === 'app.chapter-view') {
        $scope.isChapterView = true;
      } else {
        $scope.isChapterView = false;
      }

      $scope.data = slideService.getSlideMetadata();

      $scope.$watch('data.slides', function () {
        if ($scope.data.slides.length > 0) {
          // This means the metadata of all slides has been loaded completely by the slide service,
          // which reads in a JSON file.
          $scope.slideCount = slideService.getSlideCount();
          
          if (deferredSlideIndex) {
            $scope.goToSlide(deferredSlideIndex);
            deferredSlideIndex = null;
          } else {
            updateChapterClass();
          }
        }
      });

      $scope.$watch('slideIndex', function () {
        // Update slide indicators.
        $scope.isFirstSlide = ($scope.slideIndex === 0) ? true : false;
        $scope.isLastSlide = ($scope.slideIndex === ($scope.slideCount - 1)) ? true : false;
        
        updateChapterClass();
      });

      $scope.goToSlide = function (value) {
        var searchObject;
        
        // Early exit.
        if (angular.isUndefined(value)) {
          return;
        }

        if ($state.current.name === 'app.chapter-view') {
          if (angular.isNumber(value) && value >= 0) {
            if ($scope.data.slides.length === 0) {
              deferredSlideIndex = value;
              return;
            } else if (value < $scope.data.slides.length) {
              $scope.slideIndex = value;
            }
          } else if (value === 'next' && !$scope.isLastSlide) {
            $scope.slideIndex++;
          } else if (value === 'previous' && !$scope.isFirstSlide) {
            $scope.slideIndex--;
          }
          
          searchObject = $location.search();
          // Update the slide url parameter if necessary.
          if (searchObject.slide !== ($scope.slideIndex + 1).toString()) {
            // The slide url parameter is one based, so we need to add one up.
            $location.search({ 'slide': $scope.slideIndex + 1 });
          }
        } else {
          $state.go('app.chapter-view', { 'slide': value + 1 });
        }
      };
      
      $scope.backToChapterView = function () {
        $state.go('app.chapter-view');
      };
      
      $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        if (toState.name !== 'app.chapter-view') {
          updateChapterClass(0);
          $scope.isChapterView = false;
        } else {
          $scope.isChapterView = true;
        }
      });
      
      updateChapterClass = function (chapterParam) {
        var chapter, chapterClass;
        
        if (angular.isNumber(chapterParam)) {
          chapter = chapterParam;
        } else {
          chapter = slideService.getCurrentChapter($scope.slideIndex);
        }
        
        if (angular.isNumber(chapter)) {
          // First, reset last chapter class.
          // So the animation starts with the correct last chapter.
          if (angular.isString($scope.currentChapterClass) &&
              $scope.currentChapterClass.length > 8) {
            $scope.currentChapterClass = $scope.currentChapterClass.substr(0, 8);
          }
          
          chapterClass = 'chapter' + chapter;
          if (angular.isNumber(lastChapter)) {
            chapterClass += ' last-chapter' + lastChapter;
          }
          
          // Hacky, but acceptable for now (TODO).
          $timeout(function () {
            $scope.currentChapterClass = chapterClass;
          });
          lastChapter = chapter;
        }
      };
      
      // We need to call this from the chapter controller.
      $scope.updateChapterClass = updateChapterClass;
      
      $scope.supportSvg = function () {
        return (Modernizr && Modernizr.svg) ? true : false;
      };
      
      $scope.supportInlineSvg = function () {
        return (Modernizr && Modernizr.inlinesvg) ? true : false;
      };
    }]);