/* Intro Controller */

/*jslint plusplus: true */
/*global angular: true, Modernizr : true, console: true */

angular.module('eLearningApp.controllers')
  .controller('IntroController', [
    '$scope',
    function ($scope) {
      'use strict';
      
      $scope.supportSvg = function () {
        return (Modernizr && Modernizr.svg) ? true : false;
      };
      
      $scope.supportInlineSvg = function () {
        return (Modernizr && Modernizr.inlinesvg) ? true : false;
      };
    }]);