/* Eat Click Controller */

/*jslint plusplus: true, nomen: true */
/*global angular: true, console: true */

(function (angular) {
  'use strict';
  
  angular.module('eLearningApp.directives')
    .directive('eatClick', [
      '$timeout',
      function ($timeout) {
        return function (scope, element, attrs) {
          angular.element(element).on('click', function (event) {
            event.preventDefault();
          });
        };
      }]);
}(angular));