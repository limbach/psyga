/* Page Indicator Controller */

/*jslint plusplus: true, nomen: true */
/*global angular: true, $: true, Modernizr : true, console: true */

(function (angular) {
  'use strict';
  
  angular.module('eLearningApp.directives')
    .directive('pageIndicator', function () {
      var drawIndicator, isFallbackInitialized, textSpan;
    
      isFallbackInitialized = false;
    
      drawIndicator = function (canvas, index, count) {
        var context, x, y, radius, lineWidth, startAngle, endAngle;
        
        // Make sure all parameters are defined correctly before we proceed.
        if (!angular.isElement(canvas) ||
            !angular.isNumber(index) ||
            !angular.isNumber(count)) {
          return;
        }
        
        if (Modernizr && Modernizr.canvas) {
          context = canvas.getContext('2d');
          x = canvas.width / 2;
          y = canvas.height / 2;
          lineWidth = 14;
          radius = Math.min(x - (lineWidth / 2), y - (lineWidth / 2));
          startAngle = -0.5 * Math.PI;
          endAngle = ((index + 1) / count) * 2 * Math.PI - 0.5 * Math.PI;

          // Clear drawing area first.
          context.clearRect(0, 0, canvas.width, canvas.height);

          // Background arc.
          context.beginPath();
          context.arc(x, y, radius, 0, 2 * Math.PI, false);
          context.lineWidth = lineWidth;
          context.strokeStyle = '#eee';
          context.stroke();

          // Foreground arc.
          context.beginPath();
          context.arc(x, y, radius, startAngle, endAngle, false);
          context.lineWidth = lineWidth;
          context.strokeStyle = '#888';
          context.stroke();

          // Draw text.
          context.font = 'bold 16px Arial';
          context.fillStyle = '#888';
          context.textAlign = 'center';
          context.textBaseline = 'middle';
          context.fillText(index + 1, x, y);
          
          // Update tooltip.
          $(canvas).attr('title', (index + 1).toString() + ' von ' + count.toString());
        } else {
          // Show fallback text
          if (!isFallbackInitialized) {
            $(canvas).hide();
            $(canvas).parent().css({
              'width': '80px',
              'font-weight': 'bold',
              'font-size': '16px',
              'font-family': 'Arial',
              'text-align': 'center',
              'line-height': '65px',
              'color': '#888'
            });
            
            textSpan = $('<span/>').insertAfter(canvas);
            
            isFallbackInitialized = true;
          }
          
          textSpan.text((index + 1).toString() + ' von ' + count.toString());
        }
      };
    
      return {
        restrict: 'A',
        template: '<canvas height="65" width="65"></canvas>',
        scope: {
          index: '=?pageIndicatorIndex',
          count: '=?pageIndicatorMax'
        },
        link: function (scope, iElement, iAttrs) {
          var canvas;
          
          iElement.addClass('page-indicator');
          canvas = iElement.find('canvas')[0];
          
          // NOTE: Legacy AngularJS 1.2.x doesn't support $watchGroup,
          // so we need to install two separate watches.
          scope.$watch('index', function () {
            drawIndicator(canvas, scope.index, scope.count);
          });
          
          scope.$watch('count', function () {
            drawIndicator(canvas, scope.index, scope.count);
          });
        }
      };
    });
}(angular));