/* App Main File */

/*jslint plusplus: true */
/*global angular: true, console: true */

// Create module objects.
angular.module('eLearningApp.controllers', []);
angular.module('eLearningApp.directives', []);
angular.module('eLearningApp.services', []);

// Define app module.
angular.module('eLearningApp', ['ngAnimate', 'ui.router', 'eLearningApp.controllers', 'eLearningApp.directives', 'eLearningApp.services'])
  .config(function ($stateProvider, $urlRouterProvider) {
    'use strict';
  
    // Hack for IE
    document.onselectstart = function () {
      return false;
    };
  
    $stateProvider
      .state('intro', {
        url: '/intro',
        templateUrl: 'templates/legacy/intro.html',
        controller: 'IntroController'
      })
      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/legacy/app.html',
        controller: 'AppController'
      })
      .state('app.chapter-view', {
        url: '/chapter-view?slide',
        reloadOnSearch: false,
        views: {
          'content': {
            templateUrl: 'templates/legacy/chapter-view.html',
            controller: 'ChapterController'
          }
        }
      });
  
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/intro');
  });
