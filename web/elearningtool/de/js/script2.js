/**
 * Script-JS psyGA E-Learning-Tool v1.0.0
 * Copyright (c) 2012 Institut für Arbeitsmedizin und Sozialmedizin Aachen
 *
 * Date: 2013-11-19
 */

// Globale Variablen und Funktionen
var currentPage = 1;
var lastPage;
var maxPage = 50;
var quizCompleted = {"quiz1": false, "quiz2": false, "quiz3": false, "quiz4": false};
var quiz4Count = 0;
var legacy;

/**
 * Zeichenkette umdrehen
 */
String.prototype.reverse=function(){return this.split("").reverse().join("");}
/**
 * Farbraum-Konvertierung: HSL- in RGB-Werte umwandeln
 */
function hslToRgb(h, s, l) {
		h = (h % 360) / 360;

		var m2 = l <= 0.5 ? l * (s + 1) : l + s - l * s;
		var m1 = l * 2 - m2;

		return [hue(h + 1/3) * 255, hue(h) * 255, hue(h - 1/3) * 255];

		function hue(h) {
				h = h < 0 ? h + 1 : (h > 1 ? h - 1 : h);
				if (h * 6 < 1) return m1 + (m2 - m1) * h * 6;
				else if (h * 2 < 1) return m2;
				else if (h * 3 < 2) return m1 + (m2 - m1) * (2/3 - h) * 6;
				else return m1;
		}
}
/**
 * Wird bei $(window).resize aus aufgerufen und dient der vertikalen Ausrichtung des Webviews
 */
function alignWebview() {
	var offset = ($(window).height() - 768) / 2;
	offset -= 1;
	$('#webview').css('top', offset + 'px');
}

// jQuery Document-Ready-Event
$(function() {
	
	alignWebview();
	setup();
	initPage();
	
	/**
	 * Einmalige Aktionen, die beim Start der App durchgeführt werden müssen.
	 */
	function setup() {
		// Seitenanzeige (unten rechts in der Navigationsleiste) aktivieren
		$('#knob').knob({
			'thickness': .4,
			'fgColor': '#888888',
			'readOnly': true,
			'max': maxPage,
			'width': 65,
			'height': 65
		});
		if(legacy) {
			$('#knob').attr('readonly', 'readonly');
			$('#knob').wrap('<div style="display:inline-block;width:80px;height:65px;" />');
			$('#knob').css({
				'background': 'one repeat scroll 0 0 transparent',
				'border': '0 none',
				'color': '#888888',
				'font': 'bold 16px Arial',
				'height': '21px',
				'margin-left': '0px',
				'margin-top': '21px',
				'padding': '0',
				'position': 'absolute',
				'text-align': 'center',
				'vertical-align': 'middle',
				'width': '80px'
				});
		}
		
		// Laden und Zuweisen der Links für das Navigationsmenü
		$.getJSON("js/chapters.json", function(data) {
			$('#intro').attr('href', data.intro.link);
			$('#chapter1').attr('href', data.chapter1.link);
			$('#chapter2').attr('href', data.chapter2.link);
			$('#chapter3').attr('href', data.chapter3.link);
			$('#chapter4').attr('href', data.chapter4.link);
			$('#outro').attr('href', data.outro.link);
			$('#quiz').attr('href', data.quiz.link);
			$('#downloads').attr('href', data.downloads.link);
		});
	}
	
	/**
	 * Aktionen, die bei jedem Seitenwechsel durchgeführt werden müssen.
	 */
	function initPage() {
		// Seitenanzeige mit Wert aus currentPage intialisieren.
		if(!legacy) {
			$('#knob').val(currentPage).trigger('change');
		} else {
			$('#knob').val(currentPage + ' von ' + maxPage).trigger('change');
		}
		
		// Falls wir nicht auf der ersten Seite sind: Navigations-Link setzen
		if(currentPage > 1) {
			$('#back').attr('href', 'page' + (currentPage - 1) + '.html');
			$('#back').parent().removeClass('disabled');
		} else {
			$('#back').parent().addClass('disabled');
			$('#back').attr('href', 'javascript:void(0)');
		}
		
		// Falls wir nicht auf der letzten Seite sind: Navigations-Link setzen
		if(currentPage < maxPage) {
			$('#next').attr('href', 'page' + (currentPage + 1) + '.html');
			$('#next').parent().removeClass('disabled');
		} else {
			$('#next').parent().addClass('disabled');
			$('#next').attr('href', 'javascript:void(0)');
		}
		
		// Spezialhintergrund für Selbstchecks ggf. einblenden bzw. entfernen
		var checkback  = ($('#content').find('#selbstcheck1, #selbstcheck2, #selbstcheck3').length != 0) ? true : false;
		if(checkback) {
			$('#webview').prepend('<div id="checkback"></div>');
		} else {
			$('#checkback').remove();
		}
		
		// Einblenden von Überschrift und Inhaltsbereich.
		// Mit einem kleinen Timeout von 25ms um sporadisch auftretende Animationsfehler zu vermeiden.
		setTimeout( function() {
			$('#overview, #content').addClass('in');
		}, 25);
		
		// (hacky) Aktivieren des Sliders (momentan nur Seite 8)
		$('#slider').slider({
			value: 50,
			// Ändern der Hintergundfrabe des Sliders, während des Ziehens
			slide: function(event, ui) {
				var range1 = 126 - 5;
				var range2 = 88 - 62;
				var multiplier = (100 - ui.value) / 100;
				
				var value1 = Math.round(multiplier * range1) + 5;
				var value2 = 88 - Math.round(multiplier * range2);
				//$(ui.handle).parent().css('background-color', 'hsl(' + value1 + ', ' + value2 + '%, 50%)');
				var rgbValues = hslToRgb(value1, value2/100, 0.5);
				$(ui.handle).parent().css('background-color', 'rgb(' + Math.round(rgbValues[0]) + ', ' + Math.round(rgbValues[1]) + ', ' + Math.round(rgbValues[2]) + ')');
			}
		});
		$('#slider').css('background-color', '#CCDF1F'); // gelb-grün als Ausgangswert
		
		// (hacky) Ab Seite 45 beginnt das Quiz. Führe den Intialisierungscode für das Quiz aus.
		if(currentPage >= 45) {
			initQuiz();
		}
	}
	
	/**
	 * Gesonderter Initialisierungscode für die Quiz-Seiten.
	 */
	function initQuiz() {
		// HTML-Markup für die Overlay-Tastatur laden.
		$.ajax({
			url: 'js/quiz.html',
			dataType: 'html',
			success: function(data) {
				if($('#resultwrapper').length == 0) {
					$('#webview').after(data);
				}
			}
		});
		
		if(quizCompleted.quiz1) {
			$('#quiz-check1').html('<img src="img/quiz-ok.png" alt="" />');
		}
		if(quizCompleted.quiz2) {
			$('#quiz-check2').html('<img src="img/quiz-ok.png" alt="" />');
		}
		if(quizCompleted.quiz3) {
			$('#quiz-check3').html('<img src="img/quiz-ok.png" alt="" />');
		}
		if(quizCompleted.quiz4) {
			$('#quiz-check4').html('<img src="img/quiz-ok.png" alt="" />');
		}
		if(quizCompleted.quiz1 && quizCompleted.quiz2 && quizCompleted.quiz3 && quizCompleted.quiz4) {
			$('#zertifikat').html('Sie haben alle Quizfragen erfolgreich bearbeitet. Öffnen Sie Ihr <a href="pdf/psyGA_Zertifikat.pdf" target="_blank">Zertifikat</a> um es zu speichern und auszudrucken!');
		}
		
		// Reihenfolge überprüfen (Quiz 3)
		$('#sortable').sortable({
			stop: function () {
				$listElements = $('#sortable').find('li');
				var fault = false;
				$listElements.each( function (index) {
					if(('item-' + (index+1)) != $(this).attr("id")) {
						fault = true;
					}
				});
				if(!fault) {
					$("#sortable").sortable('destroy');
					$listElements.addClass('disabled');
					$listElements.find('span.quiz-ok2').addClass('in');
					
					quizCompleted.quiz3 = true;
					// TODO: Meldung ausgeben?
				}
			}
		});
		$('#sortable').disableSelection();
		
		// Drag & Drop (Quiz 4)
		$('.box-drag').draggable({revert: 'invalid', containment: '#content'});
		$('#answer1').droppable({
			accept: '#word_3',
			drop: function( event, ui ) {
				$(this).addClass('disabled');
				$(this).text(ui.draggable.text());
				$('<div style="padding-top: 8px;"><img src="img/quiz-ok.png" alt="" /></div>').appendTo(this);
				ui.draggable.remove();
				quiz4Count++;
				if(quiz4Count == 5) {
					quizCompleted.quiz4 = true;
				}
			}
		});
		$('#answer2').droppable({
			accept: '#word_1',
			drop: function( event, ui ) {
				$(this).addClass('disabled');
				$(this).text(ui.draggable.text());
				$('<div style="padding-top: 8px;"><img src="img/quiz-ok.png" alt="" /></div>').appendTo(this);
				ui.draggable.remove();
				quiz4Count++;
				if(quiz4Count == 5) {
					quizCompleted.quiz4 = true;
				}
			}
		});
		$('#answer3').droppable({
			accept: '#word_2',
			drop: function( event, ui ) {
				$(this).addClass('disabled');
				$(this).text(ui.draggable.text());
				$('<div style="padding-top: 8px;"><img src="img/quiz-ok.png" alt="" /></div>').appendTo(this);
				ui.draggable.remove();
				quiz4Count++;
				if(quiz4Count == 5) {
					quizCompleted.quiz4 = true;
				}
			}
		});
		$('#answer4').droppable({
			accept: '#word_5',
			drop: function( event, ui ) {
				$(this).addClass('disabled');
				$(this).text(ui.draggable.text());
				$('<div style="padding-top: 8px;"><img src="img/quiz-ok.png" alt="" /></div>').appendTo(this);
				ui.draggable.remove();
				quiz4Count++;
				if(quiz4Count == 5) {
					quizCompleted.quiz4 = true;
				}
			}
		});
		$('#answer5').droppable({
			accept: '#word_4',
			drop: function( event, ui ) {
				$(this).addClass('disabled');
				$(this).text(ui.draggable.text());
				$('<div style="padding-top: 8px;"><img src="img/quiz-ok.png" alt="" /></div>').appendTo(this);
				ui.draggable.remove();
				quiz4Count++;
				if(quiz4Count == 5) {
					quizCompleted.quiz4 = true;
				}
			}
		});
	}
	
	/**
	 * Lädt eine neue Seite via AJAX und ersetzt bei Erfolg die aktuelle Seite durch die neue Seite.
	 * Dadurch ist es möglich einen Übergangseffekt zwischen zwei Seiten zu benutzen.
	 * @param pageNumber Die Nummer der Seite, die geladen werden soll.
	 */
	function changePage(pageNumber) {
		var url, nextPage;
		
		if(typeof pageNumber === 'undefined') {
			// Falls der Parameter nicht definiert wurde, nehmen wir an, dass wir zur nächsten Seite 
			// navigieren wollen.
			nextPage = currentPage + 1;
			if(nextPage > maxPage) {
				return; // Funktion verlassen
			}
		} else {
			nextPage = parseInt(pageNumber, 10);
			if(isNaN(nextPage) || nextPage > maxPage || nextPage < 1) {
				return; // Funktion verlassen
			}
		}
		
		url = 'page' + nextPage + '.html';
		
		$.ajax({
			url: url,
			dataType: 'html',
			success: function(data) {
				var $response = $(data);
				var $content = $response.find('#content');
				var $overview = $response.find('#overview');
				var $insertPoint = $('#footnav');
				
				// Alten Inhalt entfernen und neuen Inhalt einfügen
				$('#overview').remove();
				$insertPoint.before($overview);
				$('#content').remove();
				$insertPoint.after($content);
				
				$('#resultwrapper').remove();
				
				// Neue Seite initialisieren und einblenden
				lastPage = currentPage;
				currentPage = nextPage;
				initPage();
				
				// Adresszeile aktualisieren (TODO: das ist kein vollständiger History-Support)
				//window.history.pushState(null, null, url);
			}
		});
	}
	
	$(window).resize(function() {
		alignWebview();
	});
	
	// (hacky) Event-Binding für alle Links mit denen ein Seitenwechsel möglich sein soll
	$('body').on('click', '#next, #back, #intro, #outro, #chapter1, #chapter2, #chapter3, #chapter4, #quiz, #downloads, #btn-quiz, #btn-infos', function(event) {
		event.preventDefault();
		
		// (hacky) Extrahiere Seitennummer aus Seitenlink (z.B. "page7.html" oder "page10.html")
		var temp = $(this).attr('href').slice(-7,-5);
		if(temp.substr(0, 1) == 'e') {
			temp = temp.substr(1, 1);
		}
		
		// Lade neue Seite und zeige sie an.
		changePage(parseInt(temp, 10));
		
		$(this).blur();
	});
	
	// Impressum anzeigen
	$('body').on('click', '#details', function() {
		$('#details-modal').modal({remote: 'js/details.html'});
	});
	
	// Auswertung von Selbstcheck 1
	$('body').on('click', '#selbstcheck1', function() {
		var radios1 =  $('input[name="radios1"]:checked').val();
		var radios2 =  $('input[name="radios2"]:checked').val();
		var radios3 =  $('input[name="radios3"]:checked').val();
		var radios4 =  $('input[name="radios4"]:checked').val();
		var radios5 =  $('input[name="radios5"]:checked').val();
		var radios6 =  $('input[name="radios6"]:checked').val();
		var radios7 =  $('input[name="radios7"]:checked').val();
		var radios8 =  $('input[name="radios8"]:checked').val();
		var radios9 =  $('input[name="radios9"]:checked').val();
		var radios10 = $('input[name="radios10"]:checked').val();
		var sum = 0;
		$.ajax({
			url: 'js/selbstcheck1.html',
			dataType: 'html',
			success: function(data) {
				if($('#resultwrapper').length == 0) { // hacky
					$('#webview').after(data);
				}
				if(typeof radios1 === 'undefined' ||
					 typeof radios2 === 'undefined' ||
					 typeof radios3 === 'undefined' ||
					 typeof radios4 === 'undefined' ||
					 typeof radios5 === 'undefined' ||
					 typeof radios6 === 'undefined' ||
					 typeof radios7 === 'undefined' ||
					 typeof radios8 === 'undefined' ||
					 typeof radios9 === 'undefined' ||
					 typeof radios10 === 'undefined') {
					 // Test nicht vollständig ausgefüllt
					 $('#result-none').modal();
					 // Funktion verlassen
					 return;
				}
				sum += parseInt(radios1, 10)
						+  parseInt(radios2, 10)
						+  parseInt(radios3, 10)
						+  parseInt(radios4, 10)
						+  parseInt(radios5, 10)
						+  parseInt(radios6, 10)
						+  parseInt(radios7, 10)
						+  parseInt(radios8, 10)
						+  parseInt(radios9, 10)
						+  parseInt(radios10, 10);
				
				var result = Math.round(sum) / 10;
				/*
					1,0 - 2,4 = Kein Burnout
					2,5 - 4,0 = Erste Warnzeichen für die Entwicklung eines Burnouts
					4,1 - 5,4 = Schwere Burnout-Problematik
					5,5 - 7,0 = Aufsuchen professioneller Unterstützung
				*/
				if(result <= 2.4) {
					$('#result1 .result').text(result);
					$('#result1').modal();
				} else if(result <= 4) {
					$('#result2 .result').text(result);
					$('#result2').modal();
				} else if(result <= 5.4) {
					$('#result3 .result').text(result);
					$('#result3').modal();
				} else if(result <= 7) {
					$('#result4 .result').text(result);
					$('#result4').modal();
				}
			}
		});
	});
	
	// Auswertung von Selbstcheck 2
	$('body').on('click', '#selbstcheck2', function() {
			var radios1 =  $('input[name="radios1"]:checked').val();
			var radios2 =  $('input[name="radios2"]:checked').val();
			var radios3 =  $('input[name="radios3"]:checked').val();
			var radios4 =  $('input[name="radios4"]:checked').val();
			var radios5 =  $('input[name="radios5"]:checked').val();
			var radios6 =  $('input[name="radios6"]:checked').val();
			var radios7 =  $('input[name="radios7"]:checked').val();
			var sum = 0;
			$.ajax({
			url: 'js/selbstcheck2.html',
			dataType: 'html',
			success: function(data) {
					if($('#resultwrapper').length == 0) { // hacky
						$('#webview').after(data);
					}
					if(typeof radios1 === 'undefined' ||
						 typeof radios2 === 'undefined' ||
						 typeof radios3 === 'undefined' ||
						 typeof radios4 === 'undefined' ||
						 typeof radios5 === 'undefined' ||
						 typeof radios6 === 'undefined' ||
						 typeof radios7 === 'undefined') {
						 // Test nicht vollständig ausgefüllt
						 $('#result-none').modal();
						 // Funktion verlassen
						 return;
					}
					sum += parseInt(radios1, 10)
							+  parseInt(radios2, 10)
							+  parseInt(radios3, 10)
							+  parseInt(radios4, 10)
							+  parseInt(radios5, 10)
							+  parseInt(radios6, 10)
							+  parseInt(radios7, 10);
					
					// Ergebnis auf eine Nachkommastelle runden
					var result = Math.round((sum / 7) * 10) / 10;
					
					if(result <= 2.2) {
						$('#result1 .result').text(result);
						$('#result1').modal();
					} else if(result <= 4.1) {
						$('#result2 .result').text(result);
						$('#result2').modal();
					} else {
						$('#result3 .result').text(result);
						$('#result3').modal();
					}
				}
			});
		});
		
		// Auswertung von Selbstcheck 3
		$('body').on('click', '#selbstcheck3', function() {
			var select1 =  $('#select1').val().substr(0, 1);
			var select2 =  $('#select2').val().substr(0, 1);
			var select3 =  $('#select3').val().substr(0, 1);
			var select4 =  $('#select4').val().substr(0, 1);
			var select5 =  $('#select5').val().substr(0, 1);
			var select6 =  $('#select6').val().substr(0, 1);
			var select7 =  $('#select7').val().substr(0, 1);
			var select8 =  $('#select8').val().substr(0, 1);
			var sum = 0;
			$.ajax({
			url: 'js/selbstcheck3.html',
			dataType: 'html',
			success: function(data) {
					if($('#resultwrapper').length == 0) {
						$('#webview').after(data);
					}
					sum += parseInt(select1, 10)
							+  parseInt(select2, 10)
							+  parseInt(select3, 10)
							+  parseInt(select4, 10)
							+  parseInt(select5, 10)
							+  parseInt(select6, 10)
							+  parseInt(select7, 10)
							+  parseInt(select8, 10);
							
					var result = sum;
					
					if(result <= 17) {
						$('#result1 .result').text(result);
						$('#result1').modal();
					} else if(result <= 31) {
						$('#result2 .result').text(result);
						$('#result2').modal();
					} else {
						$('#result3 .result').text(result);
						$('#result3').modal();
					}
				}
			});
		});
		
		// (hacky) Logik für Seite 37 (STOPP! Ein Moment zum Innehalten)
		$('body').on('click', '#stopp_a0, #stopp_a1, #stopp_a2, #stopp_a3, #stopp_a4, #stopp_a5', function() {
			if(this.id == 'stopp_a0') {
				$('#stopp0').hide();
				$('#stopp1').show();
				$('#stopp2').hide();
				$('#stopp3').hide();
			} else if(this.id == 'stopp_a1' || this.id == 'stopp_a2' || this.id == 'stopp_a3') {
				$('#stopp0').hide();
				$('#stopp1').hide();
				$('#stopp2').show();
				$('#stopp3').hide();
			} else {
				$('#stopp0').hide();
				$('#stopp1').hide();
				$('#stopp2').hide();
				$('#stopp3').show();
			}
		});
		
		// Quiz-Variablen
		var $currentPopover;
		var emptyBlock = $("<div/>").html('&#20;').text(); // hacky
		
		/**
		 * Diese Funktion überprüft, ob das richtige Lösungswort eingegeben wurde und deaktiviert 
		 * daraufhin ggf. die entsprechenden Lösungskästchen. Die Variable currentPopover bezeichnet das
		 * aktuell aktive Lösungskästchen.
		 */
		function checkCurrentWord() {
			var word1 = $currentPopover.prevUntil(':not(.quiz-block)').text();
			var word2 = $currentPopover.nextUntil(':not(.quiz-block)').text();
			var word = word1.reverse() + $currentPopover.text() + word2;
			
			var wordID = $currentPopover.parent().attr("id");
			var currentWord;
			if (wordID == 'word1') {
				currentWord = "RESSOURCEN";
			} else if (wordID == 'word2') {
				currentWord = "PSYCHE";
			} else if (wordID == 'word3') {
				currentWord = "KONTINUUM";
			} else if (wordID == 'word4') {
				currentWord = "DEPRESSION";
			} else if (wordID == 'word5') {
				currentWord = "BELASTUNGEN";
			} else if (wordID == 'word6') {
				currentWord = "PSYGA";
			}
			
			if (word == currentWord) {
				// Event-Binding entfernen
				// und CSS-Klasse für deaktivierten Status zuweisen.
				$('#' + wordID + ' .quiz-block').off('click')
						.removeClass('quiz-block')
						.addClass('quiz-block-disabled');
				
				// Entsprechendes grünes OK-Häkchen einblenden
				$('#quiz-ok-' + wordID).addClass('in');
				
				// (hacky) Testen, ob alle Lösungswörter der aktuellen Seite gefunden wurden
				if($('.quiz-block').length == 0) {
					if(currentWord == "PSYGA") {
						quizCompleted.quiz2 = true;
					} else {
						quizCompleted.quiz1 = true;
					}
					// TODO: Meldung ausgeben?
				}
			}
		}
		
		// Click-Event für Lösungskästchen: Öffne Overlay-Tastatur (Popover).
		$('body').on('click', '.quiz-block', function(e) {
			e.stopPropagation();
			
			if(!(typeof $currentPopover === 'undefined')) {
				if(($currentPopover[0] == $(this)[0]) && ($('#popover').hasClass('in'))) {
					// Popover verstecken
					$('#popover').removeClass('in');
					$currentPopover.removeClass('pulse');
					return;
				}
				
				$currentPopover.removeClass('pulse');
			}
			
			$currentPopover = $(this);
			// Berechne Position für aktuelles Popover
			var offset = $(this).offset();
			var newLeft = offset.left + $(this).outerWidth() / 2 - $('#popover').outerWidth() / 2;
			var $content = $('#content');
			var difference = newLeft + $('#popover').outerWidth() - ($content.offset().left + $content.outerWidth());
			var $arrow = $('#popover').find('.arrow');
			$arrow.removeAttr('style');
			
			if (difference > 0) {
				newLeft = newLeft - (difference);
				var arrowOldLeft = $arrow.css('left');
				arrowOldLeft = parseInt(arrowOldLeft, 10);
				var arrowOldLeft = $arrow.css('left', arrowOldLeft + difference);
			}
			
			$('#popover').css({
				'top': offset.top - $('#popover').outerHeight(),
				'left': newLeft
			});
			
			$('#popover').addClass('in');
			$currentPopover.addClass('pulse');
		});
		
		// Click-Event für Tasten der Overlay-Tastatur
		$('body').on('click', '.keys button', function(e){
			e.stopPropagation();
			var letter = $(this).text();
			
			$currentPopover.children().text(letter).removeClass('whitetext');
			checkCurrentWord();
			var $next = $currentPopover.next('.quiz-block');
			if($next.length != 0) {
				$next.trigger('click');
			} else {
				$('#popover').removeClass('in');
				$currentPopover.removeClass('pulse');
			}
		});
		
		// Verhindert die Ausbreitung des Click-Events auf hierarchisch höher gelegene DOM-Elemente.
		// Das verhindert ein versehentliches Schließen der Overlay-Tastatur.
		$('body').on('click', '#popover', function(e){
			e.stopPropagation();
		});
		
		// Click Event außerhalb der Overlay-Tastatur: Schließe Popover
		$(document).on('click', function() {
			$('#popover').removeClass('in');
			if(typeof $currentPopover !== 'undefined') {
				$currentPopover.removeClass('pulse');
			}
		});
		
		// Keydown-Event für die Overlay-Tastatur (Popover)
		$('body').on('keydown', function(e) {
			if ($('#popover').hasClass('in')) {
				if (e.which == 8) { // Backspace
					if($currentPopover.children().text() == emptyBlock) {
						var $prev = $currentPopover.prev('.quiz-block');
						if($prev.length != 0) {
							$prev.trigger('click');
						}
					} else {
						$currentPopover.children().text(emptyBlock).addClass('whitetext');
					}
					return false;
				} else if (e.which == 37) { // Links
					var $prev = $currentPopover.prev('.quiz-block');
					if($prev.length != 0) {
						$prev.trigger('click');
					}
					return false;
				} else	if (e.which == 39) { // Rechts
					var $next = $currentPopover.next('.quiz-block');
					if($next.length != 0) {
						$next.trigger('click');
					}
					return false;
				} else if (e.which >= 65 && e.which <= 90) {
					// Buchstaben [a-zA-Z]
					var charValue = String.fromCharCode(e.which);
					
					$currentPopover.children().text(charValue).removeClass('whitetext');
					checkCurrentWord();
					var $next = $currentPopover.next('.quiz-block');
					if($next.length != 0) {
						$next.trigger('click');
					} else {
						$('#popover').removeClass('in');
						$currentPopover.removeClass('pulse');
					}
				}
			}
		});
		
		// CEF-Client spezifisch: Unterbinde klassiche Browser-Shortcuts
		$('html').on('keydown', function(e) {
			if (e.which == 8) { // Backspace
				e.preventDefault();
			}
			else if (e.which == 32) { // Leertaste
				e.preventDefault();
			}
			else if (e.which == 35) { // Ende
				e.preventDefault();
			}
			else if (e.which == 36) { // Pos1
				e.preventDefault();
			}
		});
});